from django.db import models

# Create your models here.
class Endpoint(models.Model):
    title=models.CharField(max_length=100)
    input_image=models.ImageField('images')
    out_image=models.ImageField('images')
    # description = models.TextField()
    # completed=models.BooleanField(default=False)

    def _str_(self):
        return self.out_image