from customdataset import CustomImageDataset
from beautifultable import BeautifulTable
import copy
import time
import torch
import numpy as np
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from VGG_model import VGG
# from VGG_model_for_train import VGG_2node
# from scale_distance_of_features import scale_of_feature

import matplotlib.pyplot as plt
from confusion_matrix_210825 import confusion_matrix
from PIL import Image


print("<<<<< test start >>>>>")
# https://towardsdatascience.com/build-a-fully-production-ready-machine-learning-app-with-python-django-react-and-docker-c4d938c251e5

hyper_param_epoch = 1
hyper_param_batch = 50
hyper_param_learning_rate = 0.001
model_number = 2
model_name = ['VGG11', 'VGG13', 'VGG16', 'VGG19']

model_epoch = 100


select_dataset = 1 # 0 : train dataset, 1 : test dataset

class_names = ['bird', 'airplane', 'dog', 'cat', 'ship', 'frog', 'automobile', 'truck', 'deer', 'horse']

num_classes = 10


def image_loader(image_path, loader): # single image load
    """load image, returns cuda tensor"""
    # image_name = image_path()
    image = Image.open(image_path)
    image = loader(image).float()
    image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    return image.cuda() #assumes that you're using GPU


def output_model(image_path, class_number):

    # 나중에 이미지마다 모델이 각기 다를 수 있기 때문에 구분하는 부분 나중에 추가하기 ex) fullshot은 vgg이고 flower는 densenet인 경우
    if class_number == 0 : # model for flower image
        model_path = './train_weight_451_(VGG16)_best.pth'
        print("<<<<< loading flower model >>>>>>")

    elif class_number == 1 : # model for fruit image
        model_path = './train_weight_451_(VGG16)_best.pth'
        print("<<<<< loading fruit model >>>>>>")

    elif class_number == 2: # model for leaf(front) image
        model_path = './train_weight_451_(VGG16)_best.pth'
        print("<<<<< loading leaf(front) model >>>>>>")

    else : # model for leaf(back) image
        model_path = './train_weight_451_(VGG16)_best.pth'
        print("<<<<< loading leaf(back) model >>>>>>")


    print("model_image_path :",image_path)

    loader = transforms.Compose([transforms.Resize(32), transforms.ToTensor()])
    image = image_loader(image_path, loader)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    wildplant_model = VGG(m_name=model_name[model_number], num_classes=num_classes)

    # print("hidden_model :",hidden_model)

    wildplant_model.load_state_dict(torch.load(model_path))  # pretrained model load

    wildplant_model.to(device)



    wildplant_model.eval()
    with torch.no_grad():
        hidden_features, hidden_outputs = wildplant_model(image)

        # print("hidden_outputs :", hidden_outputs)
        # print("hidden_outputs len :", len(hidden_outputs[0]))

        _, predicted_eval = torch.max(hidden_outputs.data, 1)

        print("predicted_eval :", predicted_eval)
        print("predicted_class_number :", int(predicted_eval))
        print("predicted_class :", class_names[int(predicted_eval)])

        # 관련 사진 랜덤으로 5장, 성분 파일, 관련 설명 TEXT 파일 웹으로 출력하는 코드 추가하기



    print("<<<<< completed >>>>>")


# if __name__ == '__main__':
#     fullshot_path()
#     main()