from customdataset import CustomImageDataset
from beautifultable import BeautifulTable
import copy
import time
import torch
import numpy as np
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from VGG_model import VGG
from utils_for_wildplant_210827 import visualize_features
# from scale_distance_of_features import scale_of_feature

import matplotlib.pyplot as plt
from confusion_matrix_210825 import confusion_matrix
from PIL import Image

from part_classification_211119 import output_model


print("<<<<< webservice Start >>>>>")

class_names = ['bird', 'airplane', 'dog', 'cat', 'ship', 'frog', 'automobile', 'truck', 'deer', 'horse']

num_classes = 10

manual_mode = class_names[0]
automatic_mode = 123

hyper_param_epoch = 1
hyper_param_batch = 50
hyper_param_learning_rate = 0.001
model_number = 2
model_name = ['VGG11', 'VGG13', 'VGG16', 'VGG19']


#image_path는 웹에서 업로드된 이미지를 특정 폴더에 저장한 후에 그 이미지를 불러오는 것으로 하기
#웹서비스를 통해 저장된 이미지는 자동으로 1시간이내에 삭제되도록 코드 추가하기

image_path = "P://Datasets//CIFAR10_edit//train//bird//0034.png" # index number = 0
# image_path = "P://Datasets//CIFAR10_edit//train//airplane//0015.png" # index number = 1
# image_path = "P://Datasets//CIFAR10_edit//train//dog//0025.png" # index number = 2
# image_path = "P://Datasets//CIFAR10_edit//train//cat//0031.png" # index number = 3

model_path = './train_weight_451_(VGG16)_best.pth'


def image_loader(image_name, loader): # single image load
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    return image.cuda() #assumes that you're using GPU

def main():

    loader = transforms.Compose([transforms.Resize(32), transforms.ToTensor()])
    image = image_loader(image_path, loader)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


    print("num_classes :", num_classes)
    print("classe_names :", class_names)


    wildplant_model = VGG(m_name=model_name[model_number], num_classes=num_classes)

    # print("hidden_model :",hidden_model)

    wildplant_model.load_state_dict(torch.load(model_path))  # pretrained model load

    wildplant_model.to(device)


    features_loader = []
    labels_loader = []

    wildplant_model.eval()
    with torch.no_grad():


        hidden_features, hidden_outputs = wildplant_model(image)
        # print("hidden_features :",hidden_features)
        print("hidden_outputs :",hidden_outputs)
        print("hidden_outputs len :", len(hidden_outputs[0]))

        _, predicted_eval = torch.max(hidden_outputs.data, 1)


        print("predicted_eval :",predicted_eval)
        print("predicted_class_number :", int(predicted_eval))
        print("predicted_class :", class_names[int(predicted_eval)])
        # print(image_path)

        output_model(image_path, int(predicted_eval))


if __name__ == '__main__':
    main()