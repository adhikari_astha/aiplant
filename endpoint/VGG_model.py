from torch import nn
import torch
import numpy as np

cfg = {
    'VGG11': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'VGG13': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'VGG16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'VGG19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}


class VGG(nn.Module):
    def __init__(self,m_name, num_classes):
        super(VGG, self).__init__()


        self.features = self._make_layers(cfg[m_name], batch_norm=True)


        self.classifier = nn.Linear(512, num_classes)



    def forward(self, x, target=None):
        # self.fix_weights()
        out = self.features(x)
        # print("out1 :",out.shape)

        out = out.view(out.size(0), -1)
        # print("out2 :", out.shape)


        fc_features = out

        output = self.classifier(fc_features)

        return fc_features, output
    def _make_layers(self, cfg, batch_norm=False):
        layers = []
        in_channels = 3
        for x in cfg:
            if x == 'M':
                layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
            else:
                conv2d = nn.Conv2d(in_channels, x, kernel_size=3, padding=1)

                if batch_norm:
                    layers += [conv2d, nn.BatchNorm2d(x), nn.ReLU(inplace=True)]

                else:
                    layers += [conv2d, nn.ReLU(inplace=True)]
                in_channels = x
        layers += [nn.AvgPool2d(kernel_size=1, stride=1)]
        return nn.Sequential(*layers)

    # def fix_weights(self):
    #     changed_weight = []
    #     for a in range(0, 360, 360 // 5):
    #         phi = np.deg2rad(a + 30)
    #         x = np.cos(phi)
    #         y = np.sin(phi)
    #         changed_weight.append([x, y])
    #
    #     self.classifier2.weight = torch.nn.Parameter(torch.FloatTensor(changed_weight))