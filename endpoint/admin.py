from django.contrib import admin
from .models import Endpoint

# Register your models here.

class EndpointAdmin(admin.ModelAdmin):
    list=('title','input_image','out_image')

admin.site.register(Endpoint,EndpointAdmin)
