from django.shortcuts import render
from .serializers import EndpointSerializer
from rest_framework import viewsets
from .models import Endpoint

# Create your views here.
class EndpointView(viewsets.ModelViewSet):
    serializer_class=EndpointSerializer
    queryset=Endpoint.objects.all()

